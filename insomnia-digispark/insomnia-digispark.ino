#include "DigiMouse.h"
#define LED 1 // This may vary between board revisions
const int interval = 1; // min: Mouse move interval
const int moveDelay = 50; // ms: Delay between go and back cursor

void setup() {
  DigiMouse.begin();
}

void loop() {
  digitalWrite(LED, HIGH);
  int pix = random(1, 5); // Move 1-5 pixels
  int dice = random(3);
  if (dice == 0) {
    // Move up
      DigiMouse.moveY(pix);
      DigiMouse.delay(moveDelay);
      DigiMouse.moveY(-pix);
  } else if (dice == 1) {
    // Move down
      DigiMouse.moveY(-pix);
      DigiMouse.delay(moveDelay);
      DigiMouse.moveY(pix);
  } else if (dice == 2) {
    // Move right
      DigiMouse.moveX(pix);
      DigiMouse.delay(moveDelay);
      DigiMouse.moveX(-pix);
  } else if (dice == 3) {
    // move left
      DigiMouse.moveX(-pix);
      DigiMouse.delay(moveDelay);
      DigiMouse.moveX(pix);
  }
  digitalWrite(LED, LOW);
  DigiMouse.delay(1000UL*60*interval);
}
